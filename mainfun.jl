using Revise, JuMP, Gurobi, Test, DelimitedFiles, PlotlyJS, LaTeXStrings, JLD, BenchmarkTools

# include("ieee6bus_testsystem_data.jl")
# include("ieee30bus_testsystem_data.jl")
include("src/formatteddata.jl")
include("src/renewableenergysimulation.jl")
include("src/showboundrycase.jl")
include("src/readdatafromexcel.jl")
include("src/cuccommitmentmodel.jl")
include("src/tuccommitmentmodel.jl")
include("src/casesploting.jl")
include("src/creatfrequencyconstraints.jl")
include("src/cluster_units.jl")
# DataGen, DataBranch, DataLoad, LoadCurve, GenCost = IEEE_RTS6()
# DataGen, DataBranch, DataLoad, LoadCurve, GenCost = IEEE_RTS30()
UnitsFreqParam, WindsFreqParam, StrogeData, DataGen, GenCost, DataBranch, LoadCurve, DataLoad = readxlssheet()

config_param, units, lines, loads, stroges, NB, NG, NL, ND, NT, NC = forminputdata(
    DataGen, DataBranch, DataLoad, LoadCurve, GenCost, UnitsFreqParam, StrogeData
)

if config_param.is_WindIntegration == 1
    winds, NW = genscenario(WindsFreqParam, 2)
end

# boundrycondition(
#     NB::Int64, NL::Int64, NG::Int64, NT::Int64, ND::Int64,
#     units::unit, loads::load, lines::transmissionline, winds::wind, stroges::stroge,
# )

# A, b = creatfrequencyfittingfunction(units, winds, NG, NW)
# montecalrosimulation(units, winds, NG, NW)
# creatingfrequencyresponsesamplingdata(units, winds, NW, NG, Sampling_Statue)

p₀, pᵨ, pᵩ, seq_sr⁺, seq_sr⁻, su_cost, sd_cost, prod_cost, cost_sr⁺, cost_sr⁻ = scucmodel(
    NT, NB, NG, ND, NC, units, loads, winds, lines, config_param)

config_param, units, lines, loads, stroges, NB, NG, NL, ND, NT, NC = forminputdata(
    DataGen, DataBranch, DataLoad, LoadCurve, GenCost, UnitsFreqParam, StrogeData)

cunits, cNG, cluster_cunitsset, cluster_featurematrix = calculating_clustered_units(units, DataGen, GenCost, UnitsFreqParam)

p₀, pᵨ, pᵩ, seq_sr⁺, seq_sr⁻, su_cost, sd_cost, prod_cost, cost_sr⁺, cost_sr⁻ = refined_cscucmodel(
    NT, NB, NG, cNG, ND, NC, units, cunits, loads, winds, lines, config_param, cluster_cunitsset, cluster_featurematrix)

# plotcasestudies(p₀,pᵨ,pᵩ,seq_sr⁺,seq_sr⁻,su_cost,sd_cost,prod_cost,cost_sr⁺,cost_sr⁻,NT,NG,ND,NW,NC,)

# save("D:\\BaiduNetdiskWorkspace\\.current project\\.task 8\\.code\\.experiments\\benchmark_3\\mydata_1.jld",
#         "p₀",p₀,
#         "pᵨ",pᵨ,
#         "pᵩ",pᵩ,
#         "seq_sr⁺",seq_sr⁺,
#         "seq_sr⁻",seq_sr⁻,``
#         "su_cost",su_cost,
#         "sd_cost",sd_cost,
#         "prod_cost",prod_cost,
#         "cost_sr⁺",cost_sr⁺,
#         "cost_sr⁻",cost_sr⁻,
#         "NT", NT,
#         "NG", NG,
#         "ND", ND,
#         "NW", NW,
#         "winds", winds)
