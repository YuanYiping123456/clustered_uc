# clustered_uc

#### PART 1: This is a new repo. about (Clustered Unit Commitment) clustered_UC.

This repo. may be useful to learn Julia/JuMP or traditional UC (TUC)/clustered UC (CUC) problems.

The current version in this repo. still has some technical issues unsolved, mainly including an abnormal calculating time comparision of CUC comparing with TUC. Therefore, it is not recommended that viewers reference or use this source code directly. I will do my best to settle down these issues and reopen that.

If you want to run that, please following the next steps:
- Please make sure you have installed Julia on your computer. If not, please download Julia from https://julialang.org/ and install that. 
- Please install the necessary Pkg. by run “] add xxx”.
- Please correct the file-path of several .jl files replaced by your own environment. The paths to be further modified include these three files:
  >- line 7 in the src/readdatafromexcel.jl; 
  >- line 527 in the src/tuccommitmentmodel.jl; 
  >- line 638 in the src/cuccommitmentmodel.jl;
- Then click "Alt + Enter" and run it in VS Code. Done.

#### PART 2: File description:
- inputdata # boundary data 
  - data.xlsx # modified IEEE 6 bus test system
  - data1.xlsx # modified IEEE 118 bus test system
  - data2.xlsx # modified IEEE 118 bus test system
  - data1-cuc.xlsx # modified IEEE 118 bus test system, please refer to https://github.com/datejada/CUC-data
- res # result data
  - ben_calculation_result.txt # TUC result
  - pro_calculation_result.txt # CUC result
- src # sub-module code files
- mainfun.jl # main files

#### PART 3: Note:
Thank you very much for the kind help and powerful suggestions provided by Dr. Diego A. Tejada-Arango. His articles and pertinent advicements were very inspiring to me.

Cite:
 
G. Morales-Espana and D. A. Tejada-Arango, “Modelling the Hidden Flexibility of Clustered Unit Commitment,” arXiv e-prints, p. arXiv:1811.02622, Nov. 2018.
